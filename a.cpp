#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <sstream>
#include <thread>
//For sounds
#include <ao/ao.h>
#include <mpg123.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <glad/glad.h>
#include <FTGL/ftgl.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>

using namespace std;

using namespace std;
#define ll long long
#define mp(x,y) make_pair(x,y)
#define pr pair<int,int>
#define F first
#define S second
#define pb push_back

#define BITS 8

struct VAO {
	GLuint VertexArrayID;
	GLuint VertexBuffer;
	GLuint ColorBuffer;
	GLuint TextureBuffer;
	GLuint TextureID;

	GLenum PrimitiveMode; // GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES, GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY and GL_TRIANGLES_ADJACENCY
	GLenum FillMode; // GL_FILL, GL_LINE
	int NumVertices;
};
typedef struct VAO VAO;

struct GLMatrices {
	glm::mat4 projection;
	glm::mat4 model;
	glm::mat4 view;
	GLuint MatrixID; // For use with normal shader
	GLuint TexMatrixID; // For use with texture shader
} Matrices;

struct FTGLFont {
	FTFont* font;
	GLuint fontMatrixID;
	GLuint fontColorID;
} GL3Font;

GLuint programID, fontProgramID, textureProgramID,textureProgramID1,textureProgramID2,textureProgramID3,textureProgramID4;

/* Function to load Shaders - Use it as it is */
GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path) {

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open())
	{
		std::string Line = "";
		while(getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	cout << "Compiling shader : " <<  vertex_file_path << endl;
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage( max(InfoLogLength, int(1)) );
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	cout << VertexShaderErrorMessage.data() << endl;

	// Compile Fragment Shader
	cout << "Compiling shader : " << fragment_file_path << endl;
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage( max(InfoLogLength, int(1)) );
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	cout << FragmentShaderErrorMessage.data() << endl;

	// Link the program
	cout << "Linking program" << endl;
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage( max(InfoLogLength, int(1)) );
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	cout << ProgramErrorMessage.data() << endl;

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

static void error_callback(int error, const char* description)
{
	cout << "Error: " << description << endl;
}

void quit(GLFWwindow *window)
{
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

glm::vec3 getRGBfromHue (int hue)
{
	float intp;
	float fracp = modff(hue/60.0, &intp);
	float x = 1.0 - abs((float)((int)intp%2)+fracp-1.0);

	if (hue < 60)
		return glm::vec3(1,x,0);
	else if (hue < 120)
		return glm::vec3(x,1,0);
	else if (hue < 180)
		return glm::vec3(0,1,x);
	else if (hue < 240)
		return glm::vec3(0,x,1);
	else if (hue < 300)
		return glm::vec3(x,0,1);
	else
		return glm::vec3(1,0,x);
}

/* Generate VAO, VBOs and return VAO handle */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* color_buffer_data, GLenum fill_mode=GL_FILL)
{
	struct VAO* vao = new struct VAO;
	vao->PrimitiveMode = primitive_mode;
	vao->NumVertices = numVertices;
	vao->FillMode = fill_mode;

	// Create Vertex Array Object
	// Should be done after CreateWindow and before any other GL calls
	glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
	glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
	glGenBuffers (1, &(vao->ColorBuffer));  // VBO - colors

	glBindVertexArray (vao->VertexArrayID); // Bind the VAO
	glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
	glVertexAttribPointer(
						  0,                  // attribute 0. Vertices
						  3,                  // size (x,y,z)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	glBindBuffer (GL_ARRAY_BUFFER, vao->ColorBuffer); // Bind the VBO colors
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), color_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
	glVertexAttribPointer(
						  1,                  // attribute 1. Color
						  3,                  // size (r,g,b)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	return vao;
}

/* Generate VAO, VBOs and return VAO handle - Common Color for all vertices */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat red, const GLfloat green, const GLfloat blue, GLenum fill_mode=GL_FILL)
{
	GLfloat* color_buffer_data = new GLfloat [3*numVertices];
	for (int i=0; i<numVertices; i++) {
		color_buffer_data [3*i] = red;
		color_buffer_data [3*i + 1] = green;
		color_buffer_data [3*i + 2] = blue;
	}

	return create3DObject(primitive_mode, numVertices, vertex_buffer_data, color_buffer_data, fill_mode);
}

struct VAO* create3DTexturedObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* texture_buffer_data, GLuint textureID, GLenum fill_mode=GL_FILL)
{
	struct VAO* vao = new struct VAO;
	vao->PrimitiveMode = primitive_mode;
	vao->NumVertices = numVertices;
	vao->FillMode = fill_mode;
	vao->TextureID = textureID;

	// Create Vertex Array Object
	// Should be done after CreateWindow and before any other GL calls
	glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
	glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
	glGenBuffers (1, &(vao->TextureBuffer));  // VBO - textures

	glBindVertexArray (vao->VertexArrayID); // Bind the VAO
	glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
	glVertexAttribPointer(
						  0,                  // attribute 0. Vertices
						  3,                  // size (x,y,z)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	glBindBuffer (GL_ARRAY_BUFFER, vao->TextureBuffer); // Bind the VBO textures
	glBufferData (GL_ARRAY_BUFFER, 2*numVertices*sizeof(GLfloat), texture_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
	glVertexAttribPointer(
						  2,                  // attribute 2. Textures
						  2,                  // size (s,t)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	return vao;
}

/* Render the VBOs handled by VAO */
void draw3DObject (struct VAO* vao)
{
	// Change the Fill Mode for this object
	glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

	// Bind the VAO to use
	glBindVertexArray (vao->VertexArrayID);

	// Enable Vertex Attribute 0 - 3d Vertices
	glEnableVertexAttribArray(0);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

	// Enable Vertex Attribute 1 - Color
	glEnableVertexAttribArray(1);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->ColorBuffer);

	// Draw the geometry !
	glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle
}

void draw3DTexturedObject (struct VAO* vao)
{
	// Change the Fill Mode for this object
	glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

	// Bind the VAO to use
	glBindVertexArray (vao->VertexArrayID);

	// Enable Vertex Attribute 0 - 3d Vertices
	glEnableVertexAttribArray(0);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

	// Bind Textures using texture units
	glBindTexture(GL_TEXTURE_2D, vao->TextureID);

	// Enable Vertex Attribute 2 - Texture
	glEnableVertexAttribArray(2);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->TextureBuffer);

	// Draw the geometry !
	glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle

	// Unbind Textures to be safe
	glBindTexture(GL_TEXTURE_2D, 0);
}

/* Create an OpenGL Texture from an image */
GLuint createTexture (const char* filename)
{
	GLuint TextureID;
	// Generate Texture Buffer
	glGenTextures(1, &TextureID);
	// All upcoming GL_TEXTURE_2D operations now have effect on our texture buffer
	glBindTexture(GL_TEXTURE_2D, TextureID);
	// Set our texture parameters
	// Set texture wrapping to GL_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering (interpolation)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Load image and create OpenGL texture
	int twidth, theight;
	unsigned char* image = SOIL_load_image(filename, &twidth, &theight, 0, SOIL_LOAD_RGB);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D); // Generate MipMaps to use
	SOIL_free_image_data(image); // Free the data read from file after creating opengl texture
	glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess it up

	return TextureID;
}


/**************************
 * Customizable functions *
 **************************/
int jumpcnt;
float triangle_rot_dir = 1;
float rectangle_rot_dir = -1;
bool triangle_rot_status = true;
bool rectangle_rot_status = true;
char output[200],level0[200],level1[200],level2[200],level3[200];
float posx,posy,posz,prevx,prevz;
double rotang,cuberot=0;
bool in_rotate,lose;
int win,view_mode=0,flag_rot,scrollLen;
bool keyup,keydown,keyleft,keyright,keyt,keyr,keyspace,keyh,keyf,keye;
GLfloat basecol[108], grey[108], white[108], bodyc[108], black[108],gold[16000],red[16000],orange[16000];


void *play_audio(string audioFile)
{
    mpg123_handle *mh;
    unsigned char *buffer;
    size_t buffer_size;
    size_t done;
    int err;

    int driver;
    ao_device *dev;

    ao_sample_format format;
    int channels, encoding;
    long rate;

    /* initializations */
    ao_initialize();
    driver = ao_default_driver_id();
    mpg123_init();
    mh = mpg123_new(NULL, &err);
    buffer_size = mpg123_outblock(mh);
    buffer = (unsigned char*) malloc(buffer_size * sizeof(unsigned char));

    /* open the file and get the decoding format */
    mpg123_open(mh, &audioFile[0]);
    mpg123_getformat(mh, &rate, &channels, &encoding);

    /* set the output format and open the output device */
    format.bits = mpg123_encsize(encoding) * 8;
    format.rate = rate;
    format.channels = channels;
    format.byte_format = AO_FMT_NATIVE;
    format.matrix = 0;
    dev = ao_open_live(driver, &format, NULL);

    /* decode and play */
    char *p =(char *)buffer;
    while (mpg123_read(mh, buffer, buffer_size, &done) == MPG123_OK)
        ao_play(dev, p, done);

    /* clean up */
    free(buffer);
    ao_close(dev);
    mpg123_close(mh);
    mpg123_delete(mh);
}


void cbfun (GLFWwindow* window, double x,double y)
{
    if(y==-1)
    {
        scrollLen+=5;
    }
    if(y==1)
    {
        scrollLen-=5;
    }
}

/* Executed when a regular key is pressed/released/held-down */
/* Prefered for Keyboard events */
void keyboard (GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// Function is called first on GLFW_PRESS.

	if (action == GLFW_RELEASE) {
		switch (key) {
			case GLFW_KEY_UP:
				keyup=false;
				break;
			case GLFW_KEY_DOWN:
				keydown=false;
				break;
			case GLFW_KEY_LEFT:
				keyleft=false;
				break;
			case GLFW_KEY_RIGHT:
				keyright=false;
				break;
			case GLFW_KEY_SPACE:
				keyspace=false;
				break;
			default:
				break;
		}
	}
	else if (action == GLFW_PRESS) {
		switch (key) {
			case GLFW_KEY_ESCAPE:
				quit(window);
				break;
			case GLFW_KEY_T:
				keyt=!keyt;
				break;
			case GLFW_KEY_R:
				keyr=true;
				break;
			case GLFW_KEY_UP:
				keyup=true;
				break;
			case GLFW_KEY_DOWN:
				keydown=true;
				break;
			case GLFW_KEY_LEFT:
				keyleft=true;
				break;
			case GLFW_KEY_RIGHT:
				keyright=true;
				break;
			case GLFW_KEY_SPACE:
				if(jumpcnt==0)
				{
					thread(play_audio,"jump.mp3").detach();
					jumpcnt=24;
				}
				keyspace=true;
				break;
			case GLFW_KEY_H:
				keyh=!keyh;
				break;
			case GLFW_KEY_F:
				keyf=!keyf;
				break;
			case GLFW_KEY_E:
				keye=!keye;
				break;
			default:
				break;
		}
	}
}

/* Executed for character input (like in text boxes) */
void keyboardChar (GLFWwindow* window, unsigned int key)
{
	switch (key) {
		case 'Q':
		case 'q':
			quit(window);
			break;
		default:
			break;
	}
}

/* Executed when a mouse button is pressed/released */
void mouseButton (GLFWwindow* window, int button, int action, int mods)
{
	switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			if(action == GLFW_PRESS)
			{
				flag_rot=1;
				in_rotate=true;
			}
			if (action == GLFW_RELEASE)
				in_rotate=false;
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			if(action == GLFW_PRESS)
			{
				flag_rot=-1;
				in_rotate=true;
			}
			if (action == GLFW_RELEASE)
				in_rotate=false;
			if (action == GLFW_RELEASE) {
				rectangle_rot_dir *= -1;
			}
			break;
		default:
			break;
	}
}


/* Executed when window is resized to 'width' and 'height' */
/* Modify the bounds of the screen here in glm::ortho or Field of View in glm::Perspective */
void reshapeWindow (GLFWwindow* window, int width, int height)
{
	int fbwidth=width, fbheight=height;
	/* With Retina display on Mac OS X, GLFW's FramebufferSize
	 is different from WindowSize */
	glfwGetFramebufferSize(window, &fbwidth, &fbheight);

	GLfloat fov = 1.6f;

	// sets the viewport of openGL renderer
	glViewport (0, 0, (GLsizei) fbwidth, (GLsizei) fbheight);

	// set the projection matrix as perspective
	 /*glMatrixMode (GL_PROJECTION);
	 glLoadIdentity ();
	 gluPerspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1, 1000.0); */
	// Store the projection matrix in a variable for future use
	// Perspective projection for 3D views
	 if(view_mode==0)
	 Matrices.projection = glm::perspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1f, 3000.0f);

	// Ortho projection for 2D views
	else if(view_mode==1)
	Matrices.projection = glm::ortho((float)(-width/2.0), (float)(width/2.0), (float)(-height/2.0), (float)(height/2.0), -1000.0f, 3000.0f);
}

VAO *triangle, *rectangle,*rectangle1,*shoe[2], *cuboid[10][10],*killer,*head,*hand[2],*leg[2],*sphere1,*sphere2,*body,*centercub, *eyes, *hair,*orbit1,*orbit2,*star,*sun,*galaxy,*galaxy2,*powerup,*rockcub,*rockhead,*fire,*bigfire,*wincoin,*earth,*moon;
glm::vec3 arrays[100];

// Creates the triangle object used in this sample code
void createTriangle ()
{
	/* ONLY vertices between the bounds specified in glm::ortho will be visible on screen */

	/* Define vertex array as used in glBegin (GL_TRIANGLES) */
	static const GLfloat vertex_buffer_data [] = {
		0, 1,0, // vertex 0
		-1,-1,0, // vertex 1
		1,-1,0, // vertex 2
	};

	static const GLfloat color_buffer_data [] = {
		1,0,0, // color 0
		0,1,0, // color 1
		0,0,1, // color 2
	};

	// create3DObject creates and returns a handle to a VAO that can be used later
	triangle = create3DObject(GL_TRIANGLES, 3, vertex_buffer_data, color_buffer_data, GL_LINE);
}

// Creates the rectangle object used in this sample code
VAO* createRectangle (GLuint textureID,int L,int B)
{
	// GL3 accepts only Triangles. Quads are not supported
	GLfloat vertex_buffer_data [] = {
		-L,-B,0, // vertex 1
		L,-B,0, // vertex 2
		L, B,0, // vertex 3

		L, B,0, // vertex 3
		-L, B,0, // vertex 4
		-L,-B,0  // vertex 1
	};

	GLfloat color_buffer_data [] = {
		1,0,0, // color 1
		0,0,1, // color 2
		0,1,0, // color 3

		0,1,0, // color 3
		0.3,0.3,0.3, // color 4
		1,0,0  // color 1
	};

	// Texture coordinates start with (0,0) at top left of the image to (1,1) at bot right
	GLfloat texture_buffer_data [] = {
		0,1, // TexCoord 1 - bot left
		1,1, // TexCoord 2 - bot right
		1,0, // TexCoord 3 - top right

		1,0, // TexCoord 3 - top right
		0,0, // TexCoord 4 - top left
		0,1  // TexCoord 1 - bot left
	};

	// create3DTexturedObject creates and returns a handle to a VAO that can be used later
	return create3DTexturedObject(GL_TRIANGLES, 6, vertex_buffer_data, texture_buffer_data, textureID, GL_FILL);
}

float formatAngle(float A)
{
	if(A < 0.0)
		A += 360.0;
	else if(A >= 360.0)
		A -= 360.0;
	return A;
}

VAO* createSector(float R, int parts)
{
	float diff = 360.0/parts;
	float A1 = formatAngle(-diff/2);
	float A2 = formatAngle(diff/2);
	GLfloat vertex_buffer [] = {
		//0,0,0,
		R*cos(A1*M_PI/180.0),R*sin(A1*M_PI/180.0),0,
		R*cos(A2*M_PI/180.0),R*sin(A2*M_PI/180.0),0
	};
	GLfloat color [] = {
		//1,0,0,
		1,0,0,
		1,0,0
	};
	return create3DObject(GL_LINES, 2, vertex_buffer, color, GL_LINE);
}

VAO* create_sphere(float radius,GLfloat color[])
{
	GLfloat vertices[3*360*180];
	int c=0;
	float space=10;
	for(float b=-180;b<180;b+=space)
	{
		for(float a=0;a<360;a+=space)
		{
			vertices[c]=sin(a*M_PI/180.0) * sin( b*M_PI/180.0)*radius;
			vertices[c+2]=cos(a*M_PI/180.0) * sin(b*M_PI/180.0)*radius;
			vertices[c+1]=cos(b*M_PI/180.0)*radius;
			
			c+=3;
			vertices[c]=sin(a*M_PI/180.0) * sin( (b+space)*M_PI/180.0)*radius;
			vertices[c+2]=cos(a*M_PI/180.0) * sin((b+space)*M_PI/180.0)*radius;
			vertices[c+1]=cos((b+space)*M_PI/180.0)*radius;
			
			c+=3;
			vertices[c]=sin((a+space)*M_PI/180.0) * sin( b*M_PI/180.0)*radius;
			vertices[c+2]=cos((a+space)*M_PI/180.0) * sin(b*M_PI/180.0)*radius;
			vertices[c+1]=cos(b*M_PI/180.0)*radius;
			
			c+=3;
			vertices[c]=sin((a+space)*M_PI/180.0) * sin((b+space)*M_PI/180.0)*radius;
			vertices[c+2]=cos((a+space)*M_PI/180.0) * sin((b+space)*M_PI/180.0)*radius;
			vertices[c+1]=cos((b+space)*M_PI/180.0)*radius;
		
			c+=3;
		}
	}
	return create3DObject(GL_TRIANGLES, 4*(360/space)*(360/space), vertices, color, GL_FILL);	
}

VAO* create_star(float l)
{
	GLfloat vertex[] = {
		0,-(sqrt(3)*l),0,
		l,l,0,
		-l,l,0,

		0,(sqrt(3)*l),0,
		-l,-l,0,
		l,-l,0
	};
	GLfloat color[]={
		1,1,1,
		1,1,1,
		1,1,1,
		1,1,1,
		1,1,1,
		1,1,1
	};
	return create3DObject(GL_TRIANGLES, 6, vertex, color, GL_FILL);	
}

VAO* createPyramid(float length,float height,GLfloat color[])
{
    GLfloat vertex_buffer_data[]={
        -length,0,length,
        -length,0,-length,
        length,0,length,
        length,0,length,
        length,0,-length,
        -length,0,-length,

        -length,0,length,
        -length,0,-length,
        0,height,0,
        -length,0,length,
        length,0,length,
        0,height,0,
        length,0,length,
        length,0,-length,
        0,height,0,
        length,0,-length,
        -length,0,-length,
        0,height,0,

       -length,0,length,
        -length,0,-length,
        0,-height,0,
        -length,0,length,
        length,0,length,
        0,-height,0,
        length,0,length,
        length,0,-length,
        0,-height,0,
        length,0,-length,
        -length,0,-length,
        0,-height,0
    };
    return create3DObject(GL_TRIANGLES, 30, vertex_buffer_data, color, GL_FILL);
}

VAO* create_halfpyramid(float length,float height,GLfloat color[])
{
    GLfloat vertex_buffer_data[]={
        -length,0,length,
        -length,0,-length,
        length,0,length,
        length,0,length,
        length,0,-length,
        -length,0,-length,

        -length,0,length,
        -length,0,-length,
        0,height,0,
        -length,0,length,
        length,0,length,
        0,height,0,
        length,0,length,
        length,0,-length,
        0,height,0,
        length,0,-length,
        -length,0,-length,
        0,height,0
    };
    return create3DObject(GL_TRIANGLES, 18, vertex_buffer_data, color, GL_FILL);
}


VAO* createCuboid(double L,double B,double H,GLfloat colors[])
{
	GLfloat vertex [] = {
		//Front Face
		-L, -H,  B,
		L, -H,  B,
		L,  H,  B,
		L,  H,  B,
		-L,  H,  B,
		-L, -H,  B,
		//Back Face
		-L, -H, -B,
		L, -H, -B,
		L,  H, -B,
		L,  H, -B,
		-L,  H, -B,
		-L, -H, -B,
		//left Face
		-L, -H,  B,
		-L, -H, -B,
		-L,  H, -B,
		-L,  H, -B,
		-L,  H,  B,
		-L, -H,  B,
		//right Face
		L, -H,  B,
		L, -H, -B,
		L,  H, -B,
		L,  H, -B,
		L,  H,  B,
		L, -H,  B,
		//Top Face
		-L,  H,  B,
		-L,  H, -B,
		L,  H, -B,
		L,  H, -B,
		L,  H,  B,
		-L,  H,  B,
		//Bottom Face
		-L, -H,  B,
		-L, -H, -B,
		L, -H, -B,
		L, -H, -B,
		L, -H,  B,
		-L, -H,  B
	};

	return create3DObject(GL_TRIANGLES, 36, vertex, colors, GL_FILL);
}

void draw_object(VAO* object,glm::vec3 translator,float angle,glm::vec3 rotate)
{
	if(keyt)
	{
		Matrices.view = glm::lookAt(glm::vec3(0,500,-1),glm::vec3(0,0,0), glm::vec3(0,1,0));
	}
	else if(keyh)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx,posy+70,posz),glm::vec3(posx-70*cos(-(cuberot)*M_PI/180.0),posy+60,posz-70*sin(-(cuberot)*M_PI/180.0)), glm::vec3(0,1,0));	
	}
	else if(keyf)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx+70*cos((rotang)*M_PI/180.0),posy+70,posz+70*sin((rotang)*M_PI/180.0)),glm::vec3(posx,posy,posz),glm::vec3(0,1,0));
	}
	else
	{
		Matrices.view = glm::lookAt(glm::vec3(350*cos(rotang*M_PI/180.0),350,(-350)*sin(rotang*M_PI/180.0)+scrollLen), glm::vec3(0,0,0), glm::vec3(0,1,0));//glm::vec3(sinf(c*M_PI/180.0),3*cosf(c*M_PI/180.0),0)); // Fixed camera for 2D (ortho) in XY plane
	}
	//Matrices.view=glm::lookAt(glm::vec3(0,0,3),glm::vec3(0,0,0),glm::vec3(0,1,0));
	glm::mat4 VP= Matrices.projection * Matrices.view;
	glm::mat4 MVP;
	Matrices.model = glm::mat4(1.0f);
	glm::mat4 translateRectangle = glm::translate (translator);        // glTranslatef
	glm::mat4 rotateRectangle = glm::rotate(angle, rotate); // rotate about vector (-1,1,1)
	Matrices.model *= (translateRectangle*rotateRectangle);// rotateRectangle);
	MVP = VP * Matrices.model;
	glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);

	// draw3DObject draws the VAO given to it using current MVP matrix
	draw3DObject(object);
}

void draw_Hero(VAO* object,glm::vec3 translator,float angle,glm::vec3 rotate,float angle2,glm::vec3 rotate2,float ang, float len)
{
	
	if(keyt)
	{
		Matrices.view = glm::lookAt(glm::vec3(0,500,-1),glm::vec3(0,0,0), glm::vec3(0,1,0));
	}
	else if(keyh)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx,posy+70,posz),glm::vec3(posx-70*cos(-(cuberot)*M_PI/180.0),posy+60,posz-70*sin(-(cuberot)*M_PI/180.0)), glm::vec3(0,1,0));	
	}
	else if(keyf)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx+70*cos((rotang)*M_PI/180.0),posy+70,posz+70*sin((rotang)*M_PI/180.0)),glm::vec3(posx,posy,posz),glm::vec3(0,1,0));
	}
	else
	{
		Matrices.view = glm::lookAt(glm::vec3(350*cos(rotang*M_PI/180.0),350,(-350)*sin(rotang*M_PI/180.0)+scrollLen), glm::vec3(0,0,0), glm::vec3(0,1,0));//glm::vec3(sinf(c*M_PI/180.0),3*cosf(c*M_PI/180.0),0)); // Fixed camera for 2D (ortho) in XY plane
	}
	
	//Matrices.view=glm::lookAt(glm::vec3(0,0,3),glm::vec3(0,0,0),glm::vec3(0,1,0));
	glm::mat4 VP= Matrices.projection * Matrices.view;
	glm::mat4 MVP;
	float A = ang*M_PI/180.0;
	Matrices.model = glm::mat4(1.0f);
	glm::mat4 moveto = glm::translate(glm::vec3(0,len,0));
	glm::mat4 movefrom = glm::translate(glm::vec3(0,-len,0));
	glm::mat4 rotto = glm::rotate(A,glm::vec3(1,0,0));
	glm::mat4 toorg = glm::translate(glm::vec3(translator[0]-posx,translator[1]-posy,translator[2]-posz));//(glm::vec3(-posx,-posy,-posz));
	glm::mat4 fromorg = glm::translate(glm::vec3(posx,posy,posz));
	glm::mat4 translateRectangle = glm::translate(translator); //(glm::vec3(translator[0]-posx,translator[1]-posy,translator[2]-posz));        // glTranslatef
	glm::mat4 rotateRectangle = glm::rotate(angle, rotate); // rotate about vector (-1,1,1)
	glm::mat4 rotator = glm::rotate(angle2,rotate2);
	Matrices.model *= (fromorg*rotator*toorg*moveto*rotto*movefrom*rotateRectangle);// rotateRectangle);
	MVP = VP * Matrices.model;

	glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
	// draw3DObject draws the VAO given to it using current MVP matrix
	draw3DObject(object);
}

void draw_3d(int q,VAO* object,int x,int y,int z,float angle,glm::vec3 rotator)
{
	if(keyt)
	{
		Matrices.view = glm::lookAt(glm::vec3(0,500,-1),glm::vec3(0,0,0), glm::vec3(0,1,0));
	}
	else if(keyh)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx,posy+70,posz),glm::vec3(posx-70*cos(-(cuberot)*M_PI/180.0),posy+60,posz-70*sin(-(cuberot)*M_PI/180.0)), glm::vec3(0,1,0));	
	}
	else if(keyf)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx+70*cos((rotang)*M_PI/180.0),posy+70,posz+70*sin((rotang)*M_PI/180.0)),glm::vec3(posx,posy,posz),glm::vec3(0,1,0));
	}
	else
	{
		Matrices.view = glm::lookAt(glm::vec3(350*cos(rotang*M_PI/180.0),350,(-350)*sin(rotang*M_PI/180.0)+scrollLen), glm::vec3(0,0,0), glm::vec3(0,1,0));//glm::vec3(sinf(c*M_PI/180.0),3*cosf(c*M_PI/180.0),0)); // Fixed camera for 2D (ortho) in XY plane
	}
	
	//Matrices.view=glm::lookAt(glm::vec3(0,0,3),glm::vec3(0,0,0),glm::vec3(0,1,0));
	glm::mat4 VP= Matrices.projection * Matrices.view;
	glm::mat4 MVP;
	// Render with texture shaders now
	if(q==0)
		glUseProgram(textureProgramID);
	else if(q==1)
		glUseProgram(textureProgramID1);
	else if(q==2)
		glUseProgram(textureProgramID2);

	// Pop matrix to undo transformations till last push matrix instead of recomputing model matrix
	// glPopMatrix ();
	Matrices.model = glm::mat4(1.0f);

	glm::mat4 translateRectangle = glm::translate (glm::vec3(x,y,z));        // glTranslatef
	glm::mat4 rotateRectangle = glm::rotate((float)angle, rotator); // rotate about vector (-1,1,1)
	Matrices.model *= (translateRectangle * rotateRectangle);
	MVP = VP * Matrices.model;

	// Copy MVP to texture shaders
	glUniformMatrix4fv(Matrices.TexMatrixID, 1, GL_FALSE, &MVP[0][0]);

	// Set the texture sampler to access Texture0 memory
	glUniform1i(glGetUniformLocation(textureProgramID, "texSampler"), 0);

	// draw3DObject draws the VAO given to it using current MVP matrix
	draw3DTexturedObject(object);
}

void draw_obj(VAO* object,glm::vec3 translator,float angle,glm::vec3 rotate,float angle2,glm::vec3 rotate2,float ang, float len)
{
	
	if(keyt)
	{
		Matrices.view = glm::lookAt(glm::vec3(0,500,-1),glm::vec3(0,0,0), glm::vec3(0,1,0));
	}
	else if(keyh)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx,posy+70,posz),glm::vec3(posx-70*cos(-(cuberot)*M_PI/180.0),posy+60,posz-70*sin(-(cuberot)*M_PI/180.0)), glm::vec3(0,1,0));	
	}
	else if(keyf)
	{
		Matrices.view = glm::lookAt(glm::vec3(posx+70*cos((rotang)*M_PI/180.0),posy+70,posz+70*sin((rotang)*M_PI/180.0)),glm::vec3(posx,posy,posz),glm::vec3(0,1,0));
	}
	else
	{
		Matrices.view = glm::lookAt(glm::vec3(350*cos(rotang*M_PI/180.0),350,(-350)*sin(rotang*M_PI/180.0)+scrollLen), glm::vec3(0,0,0), glm::vec3(0,1,0));//glm::vec3(sinf(c*M_PI/180.0),3*cosf(c*M_PI/180.0),0)); // Fixed camera for 2D (ortho) in XY plane
	}
	
	//Matrices.view=glm::lookAt(glm::vec3(0,0,3),glm::vec3(0,0,0),glm::vec3(0,1,0));
	glm::mat4 VP= Matrices.projection * Matrices.view;
	glm::mat4 MVP;
	float A = ang*M_PI/180.0;
	Matrices.model = glm::mat4(1.0f);
	glm::mat4 moveto = glm::translate(glm::vec3(0,len,0));
	glm::mat4 movefrom = glm::translate(glm::vec3(0,-len,0));
	glm::mat4 rotto = glm::rotate(A,glm::vec3(1,0,0));
	glm::mat4 toorg = glm::translate(glm::vec3(translator[0],translator[1],translator[2]));//(glm::vec3(-posx,-posy,-posz));
	glm::mat4 fromorg = glm::translate(glm::vec3(0,0,0));
	glm::mat4 translateRectangle = glm::translate(translator); //(glm::vec3(translator[0]-posx,translator[1]-posy,translator[2]-posz));        // glTranslatef
	glm::mat4 rotateRectangle = glm::rotate(angle, rotate); // rotate about vector (-1,1,1)
	glm::mat4 rotator = glm::rotate(angle2,rotate2);
	Matrices.model *= (fromorg*rotator*toorg*moveto*rotto*movefrom*rotateRectangle);// rotateRectangle);
	MVP = VP * Matrices.model;

	glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
	// draw3DObject draws the VAO given to it using current MVP matrix
	draw3DObject(object);
}

double distance(double x1,double y1,double x2,double y2)
{
	return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}
double distanceall(double x1,double y1,double z1,double x2,double y2,double z2)
{
	return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
}


float camera_rotation_angle = 90;
float rectangle_rotation = 0;
float triangle_rotation = 0;
float moveang;
float angleofrot;
bool on_rocket=true,bad;
int flag=1,globtimer,harmx,badi,harmz,starx=500,health=100,timer_rock,stary=500,starx1=-300,stary1=300,valid[10][10],harmful[10][10],score=0,rockx=500,rockz=-285,cur_level,width,height;

/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw (GLFWwindow* window)
{
	
	if(globtimer%(60*48)==200)
		thread(play_audio,"back.mp3").detach();
	globtimer++;
	if(in_rotate)
		rotang+=1*flag_rot;
	if(!lose && !on_rocket)
	{
		if(keyup || keydown)
		{
			moveang+=1*flag;
			if(moveang==30)
				flag=-1;
			else if(moveang==-30)
				flag=1;
		}
		else
		{
			moveang=0;
		}
		if(keyup)
		{
			posx-=3*cos(angleofrot*M_PI/180.0);
			posz+=3*sin(angleofrot*M_PI/180.0);
			//angleofrot=0;
			//keyup=false;
		}
		if(keydown)
		{
			posx+=3*cos(angleofrot*M_PI/180.0);
			posz-=3*sin(angleofrot*M_PI/180.0);
			//angleofrot=0;
			//keydown=false;
		}
		if(keyleft)
		{
			cuberot+=5;
			angleofrot+=5;
			//keyleft=false;
			//posx+=3*cos(rotang*M_PI/180.0);
		}
		if(keyright)
		{
			angleofrot-=5;
			cuberot-=5;
			//keyright=false;
			//posx-=3*cos(rotang*M_PI/180.0);
		}
	}

	timer_rock++;
	if(jumpcnt>0)
	{
		if(jumpcnt>=13)
			posy+=10;
		else if(jumpcnt<=12)
		{
			posy-=10;
			/*for(int i=0;i<100;i++)
			if(posy-abs(output[i]-'0')*25.0<25.0+69.0)
				posy+=10;*/
		}
		jumpcnt--;
	}


	for(int i=0;i<100;i++)
	{
		if(output[i]-'0'>1)
		{
			if(distance(posx,posz,25.0+(i/10-5.0)*50.0,25.0+(i%10-5)*50.0)<=30+20.0)
			{
			//	cout << "Hi " << i <<  " " << posx << " " << posz << " " << distance(posx,posz,25.0+(i/10.0-5.0)*50.0,25.0+(i%10-5)*50.0) << endl;
				posx=prevx;
			}
			if(distance(posx,posz,25.0f+(i/10-5)*50.0f, 25.0f+(i%10-5)*50.0f)<=30+20.0)
			{
			//	cout << "Hi " << i <<  " " << posx << " " << posz << " " << distance(posx,posz,25.0+(i/10.0-5.0)*50.0,25.0+(i%10-5)*50.0) << endl;
				posz=prevz;
			}
		}
		else if(output[i]-'0'==0 && distance(posx,posz,25.0+(i/10-5.0)*50.0,25.0+(i%10-5)*50.0)<=20.0)
		{
			posy-=4;
			lose=true;
		}
	}
	
	if(!on_rocket && (posx<-270 || posx>270 || posz<-270 || posz>270))
	{
		lose=true;
		posy-=4;
	}

	//cout << jumpcnt << " " << posx << " " << posy << " " << posz << endl;


	// clear the color and depth in the frame buffer
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// use the loaded shader program
	// Don't change unless you know what you are doing
	glUseProgram (programID);

	// Eye - Location of camera. Don't change unless you are sure!!
	glm::vec3 eye ( 350*cos(camera_rotation_angle*M_PI/180.0f), 350 , -350*sin(camera_rotation_angle*M_PI/180.0f));
	// Target - Where is the camera looking at.  Don't change unless you are sure!!
	glm::vec3 target (0, 0, 0);
	// Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
	glm::vec3 up (0, 1, 0);

	// Compute Camera matrix (view)
	 Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
	//  Don't change unless you are sure!!
	static float c = 0;
	c++;
	//Matrices.view = glm::lookAt(glm::vec3(300,300,300), glm::vec3(0,0,0), glm::vec3(0,1,0));//glm::vec3(sinf(c*M_PI/180.0),3*cosf(c*M_PI/180.0),0)); // Fixed camera for 2D (ortho) in XY plane

	// Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
	//  Don't change unless you are sure!!
	glm::mat4 VP = Matrices.projection * Matrices.view;

	// Send our transformation to the currently bound shader, in the "MVP" uniform
	// For each model you render, since the MVP will be different (at least the M part)
	//  Don't change unless you are sure!!
	glm::mat4 MVP;	// MVP = Projection * View * Model


	// Load identity to model matrix
	for(int i=0;i<10;i++)
	{
		for(int j=0;j<10;j++)
		{
			if(output[i*10+j]!='0')
			{
				Matrices.model = glm::mat4(1.0f);
				if(output[i*10+j]=='0'-1 && valid[i][j]==1 && cur_level>2)
					draw_Hero(cuboid[i][j],glm::vec3(25.0f+(i-5)*50.0f,0.0+(abs(output[i*10+j]-'0')-1)*25+(globtimer/10)%40, 25.0f+(j-5)*50.0f),0,glm::vec3(0,0,1),0,glm::vec3(0,1,0),0,0);
				else
					draw_Hero(cuboid[i][j],glm::vec3(25.0f+(i-5)*50.0f,0.0+(abs(output[i*10+j]-'0')-1)*25, 25.0f+(j-5)*50.0f),0,glm::vec3(0,0,1),0,glm::vec3(0,1,0),0,0);
			}
		}
	}
	prevx=posx;
	prevz=posz;


	draw_Hero(head,glm::vec3(posx,posy+36,posz),0,glm::vec3(0,0,1),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),0,0);
	draw_Hero(hair,glm::vec3(posx,posy+42,posz),0,glm::vec3(0,0,1),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),0,0);
	draw_Hero(eyes,glm::vec3(posx+6,posy+40,posz-15),0,glm::vec3(0,0,1),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),0,0);
	draw_Hero(eyes,glm::vec3(posx-6,posy+40,posz-15),0,glm::vec3(0,0,1),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),0,0);
	
	draw_Hero(hand[0],glm::vec3(18+posx,posy,posz),M_PI/2.0,glm::vec3(1,0,0),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),moveang,16);
	draw_Hero(hand[1],glm::vec3(-18+posx,posy,posz),M_PI/2.0,glm::vec3(1,0,0),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),-moveang,16);
	draw_Hero(body,glm::vec3(posx,posy,posz),0,glm::vec3(0,1,0),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),0,0);
	draw_Hero(leg[0],glm::vec3(8+posx,posy-28,posz),M_PI/2.0,glm::vec3(1,0,0),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),moveang,16);
	draw_Hero(leg[1],glm::vec3(-8+posx,posy-28,posz),M_PI/2.0,glm::vec3(1,0,0),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),-moveang,16);
	draw_Hero(shoe[0],glm::vec3(8+posx,posy-41,posz),M_PI/2.0,glm::vec3(1,0,0),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),moveang,0);
	draw_Hero(shoe[1],glm::vec3(-8+posx,posy-41,posz),M_PI/2.0,glm::vec3(1,0,0),(cuberot+90)*M_PI/180.0,glm::vec3(0,1,0),-moveang,0);
	
	for(int i=-6;i<=6;i++)
	{
		draw_Hero(sphere1,glm::vec3(-100,200,350),-M_PI/i,glm::vec3(0,0,1),0,glm::vec3(1,0,0),0,0);
		draw_Hero(sphere2,glm::vec3(100+650,200,400),-M_PI/i,glm::vec3(0,0,1),0,glm::vec3(1,0,0),0,0);
		draw_Hero(sun,glm::vec3(300,-200,800),-M_PI/i,glm::vec3(0,0,1),0,glm::vec3(1,0,0),0,0);
	}
	for(int i=0;i<8;i++)
		draw_object(star,arrays[i],0,glm::vec3(0,0,1));
	
	for(int i=0;i<90;i++)
	{
		draw_obj(orbit1,glm::vec3(-100,600,300),i*4*M_PI/180.0,glm::vec3(0,0,1),-M_PI/2.0,glm::vec3(1,0,0),0,0);
		draw_obj(orbit2,glm::vec3(-100,200,100),i*4*M_PI/180.0,glm::vec3(0,0,1),-M_PI/1.8,glm::vec3(1,0,0),0,0);
	}
	if(win!=cur_level)
	{
		draw_object(wincoin,glm::vec3(-225,60,225),0,glm::vec3(1,0,0));
	}
	if(distance(posx,posz,225,-225)<=16 && win==cur_level && keye)
	{
		on_rocket = true;
		timer_rock=0;
		cuberot=0;
		rotang=180;
	}
	if(on_rocket)
	{
		if(timer_rock==50 && win!=0)
			thread(play_audio,"rocket.mp3").detach();
		keyf=true;
		rockx--;
		posx=rockx,posy=20,posz=rockz;
		
		if(rockx==-440)
		{
			rockx=440;
		}
		if(rockx==225)
		{
			rotang=90;
			angleofrot=0;
			cur_level++;
			keye=false;
			keyf=false;
			lose=false;
			posx=225,posy=69,posz=-225;
			on_rocket=false;
			cuberot=0;

			cout << cur_level << endl;
			if(cur_level==0)
			{
				for(int i=0;i<100;i++)
					output[i]=level0[i];
			}
			if(cur_level==1)
			{
				for(int i=0;i<100;i++)
					output[i]=level1[i];
			}
			if(cur_level==2)
			{
				for(int i=0;i<100;i++)
					output[i]=level2[i];
			}
			if(cur_level==3)
			{
				for(int i=0;i<100;i++)
					output[i]=level3[i];	
			}
			for(int i=0;i<100;i++)
			{
				if(output[i]=='c')
				{
					valid[i/10][i%10]=1;
					output[i]=-1+'0';
				}
				else if(output[i]=='t')
				{
					harmful[i/10][i%10]=1;
					output[i]=-1+'0';
				}
				else
				{
					valid[i/10][i%10]=0;
					harmful[i/10][i%10]=0;
				}
			}
			for(int i=0;i<10;i++)
			{
				for(int j=0;j<10;j++)
				{
						cuboid[i][j]=createCuboid(25,25,25*abs((output[i*10+j])-'0'),basecol);
				}
			}
		}
	}
	draw_object(rockcub,glm::vec3(rockx,0,rockz),0,glm::vec3(0,0,1));
	draw_object(rockhead,glm::vec3(rockx-50,0,rockz),(float)(M_PI/2.0),glm::vec3(0,0,1));
	if(on_rocket)
	{
		draw_object(fire,glm::vec3(rockx+65,0,rockz-10),(float)(M_PI/2.0),glm::vec3(0,0,1));		
		draw_object(fire,glm::vec3(rockx+65,0,rockz+10),(float)(M_PI/2.0),glm::vec3(0,0,1));
		draw_object(bigfire,glm::vec3(rockx+80,0,rockz),(float)(M_PI/2.0),glm::vec3(0,0,1));
	}

	// Increment angles
	float increments = 1;


// Render font on screen
	static int fontScale = 0;
	float fontScaleValue = 50 + 25*sinf(fontScale*M_PI/180.0f);
	glm::vec3 fontColor = getRGBfromHue (fontScale);
	
	if(win<cur_level && distance(posx,posz,-225,225)<=16)
	{
		score+=500;
		thread(play_audio,"wins.mp3").detach();
		win++;
	}
	camera_rotation_angle=rotang;

	if(health<=0)
		lose=true;

	if(lose==true || posy<=-300)
	{
		posy=-300;
			// Use font Shaders for next part of code
		glUseProgram(fontProgramID);
		//	Matrices.view = glm::lookAt(glm::vec3(0,0,3), glm::vec3(0,0,0), glm::vec3(0,1,0)); // Fixed camera for 2D (ortho) in XY plane

		// Transform the text
		view_mode=1;
		reshapeWindow(window,width,height);
		Matrices.model = glm::mat4(1.0f);
		glm::mat4 translateText = glm::translate(glm::vec3(0,80,0));
		glm::mat4 scaleText = glm::scale(glm::vec3(fontScaleValue,fontScaleValue,fontScaleValue));;
		glm::mat4 rotateText = glm::rotate((float)((180)*M_PI/180.0), glm::vec3(0,1,0)); // rotate about vector (-1,1,1)
		//glm::mat4 rotatetext = glm::rotate((float)-rotang*M_PI/180.0,glm::vec3(0,0,1));
		Matrices.model *= (translateText *rotateText* scaleText);// * rotateText);
		MVP = Matrices.projection * Matrices.view * Matrices.model;
		// send font's MVP and font color to fond shaders
		glUniformMatrix4fv(GL3Font.fontMatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniform3fv(GL3Font.fontColorID, 1, &fontColor[0]);
		// Render font
		GL3Font.font->Render("You Lose!!");
		view_mode=0;
		reshapeWindow(window,width,height);
	}
	//camera_rotation_angle++; // Simulating camera rotation
	triangle_rotation = triangle_rotation + increments*triangle_rot_dir*triangle_rot_status;
	rectangle_rotation = rectangle_rotation + increments*rectangle_rot_dir*rectangle_rot_status;

	// font size and color changes
	//fontScale = (fontScale + 1) % 360;
	starx--;
	stary--;
	starx1++;
	stary1--;
	if(starx==-500)
	{
		starx=500;
		stary=500;
	}
	if(starx1==300)
	{
		starx1=-300;
		stary1=300;
	}

	for(int i=0;i<100;i++)
	{
		if( output[i]==-1+'0' && valid[i/10][i%10]==1 && distance(posx,posz,25.0+(i/10-5.0)*50.0,25.0+(i%10-5)*50.0)<=20)
		{
			valid[i/10][i%10]=0;
			score+=100;
			thread(play_audio,"coin.mp3").detach();
		}
		if(output[i]==-1+'0' && valid[i/10][i%10]==1 && cur_level>2)
			draw_object(powerup,glm::vec3(25.0+(i/10-5.0)*50.0,60+(globtimer/10)%40,25.0+(i%10-5)*50.0),0,glm::vec3(1,0,0));
		else if(output[i]==-1+'0' && valid[i/10][i%10]==1)
			draw_object(powerup,glm::vec3(25.0+(i/10-5.0)*50.0,60,25.0+(i%10-5)*50.0),0,glm::vec3(1,0,0));
		if(!bad && harmful[i/10][i%10]==1 && distance(posx,posz,25.0+(i/10-5.0)*50.0,25.0+(i%10-5)*50.0)<=20)
		{
			harmx=posx;
			harmz=posz;
			bad =true;
			badi=i;
			health-=10;
		}
		if(badi==i && harmful[i/10][i%10]==1 && distance(posx,posz,25.0+(i/10-5.0)*50.0,25.0+(i%10-5)*50.0)>20)
		{
			bad=false;
		}
		if(harmful[i/10][i%10]==1)
		{
			draw_object(killer,glm::vec3(25.0+(i/10-5.0)*50.0-10,25,25.0+(i%10-5)*50.0-10),0,glm::vec3(1,0,0));
			draw_object(killer,glm::vec3(25.0+(i/10-5.0)*50.0-10,25,25.0+(i%10-5)*50.0+10),0,glm::vec3(1,0,0));
			draw_object(killer,glm::vec3(25.0+(i/10-5.0)*50.0+10,25,25.0+(i%10-5)*50.0-10),0,glm::vec3(1,0,0));
			draw_object(killer,glm::vec3(25.0+(i/10-5.0)*50.0+10,25,25.0+(i%10-5)*50.0+10),0,glm::vec3(1,0,0));
		}
	}
	if(!on_rocket)
	{
		//To display score
		view_mode=1;
		reshapeWindow(window,width,height);

		glUseProgram(fontProgramID);
		Matrices.model = glm::mat4(1.0f);
		glm::mat4 transscore = glm::translate(glm::vec3(280,310,0));
		glm::mat4 rotscore = glm::rotate((float)((180)*M_PI/180.0),glm::vec3(0,1,0));
		glm::mat4 scalescore = glm::scale(glm::vec3(fontScaleValue,fontScaleValue,fontScaleValue));
		Matrices.model *= (transscore*scalescore*rotscore);
		MVP = Matrices.projection * Matrices.view * Matrices.model;
		glUniformMatrix4fv(GL3Font.fontMatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniform3fv(GL3Font.fontColorID, 1, &fontColor[0]);

		stringstream ss;
		ss << score;
		string text;
		ss >> text;
		char s[100] = "Score:";
		for(int i=0;i<text.length();i++)
		{
			s[i+6]=text[i];
		}
		GL3Font.font->Render(s);
		view_mode=0;
		reshapeWindow(window,width,height);
		if(1)
		{
			view_mode=1;
			reshapeWindow(window,width,height);

			glUseProgram(fontProgramID);
			Matrices.model = glm::mat4(1.0f);
			glm::mat4 transscore = glm::translate(glm::vec3(280,360,0));
			glm::mat4 rotscore = glm::rotate((float)((180)*M_PI/180.0),glm::vec3(0,1,0));
			glm::mat4 scalescore = glm::scale(glm::vec3(fontScaleValue,fontScaleValue,fontScaleValue));
			Matrices.model *= (transscore*scalescore*rotscore);
			MVP = Matrices.projection * Matrices.view * Matrices.model;
			glUniformMatrix4fv(GL3Font.fontMatrixID, 1, GL_FALSE, &MVP[0][0]);
			glUniform3fv(GL3Font.fontColorID, 1, &fontColor[0]);

			stringstream ss;
			ss << cur_level;
			string text;
			ss >> text;
			char s[100] = "Level:";
			for(int i=0;i<text.length();i++)
			{
				s[i+6]=text[i];
			}
			GL3Font.font->Render(s);
			view_mode=0;
			reshapeWindow(window,width,height);
		}
		if(1)
		{
			view_mode=1;
			reshapeWindow(window,width,height);

			glUseProgram(fontProgramID);
			Matrices.model = glm::mat4(1.0f);
			glm::mat4 transscore = glm::translate(glm::vec3(-140,360,0));
			glm::mat4 rotscore = glm::rotate((float)((180)*M_PI/180.0),glm::vec3(0,1,0));
			glm::mat4 scalescore = glm::scale(glm::vec3(fontScaleValue,fontScaleValue,fontScaleValue));
			Matrices.model *= (transscore*scalescore*rotscore);
			MVP = Matrices.projection * Matrices.view * Matrices.model;
			glUniformMatrix4fv(GL3Font.fontMatrixID, 1, GL_FALSE, &MVP[0][0]);
			glUniform3fv(GL3Font.fontColorID, 1, &fontColor[0]);

			stringstream ss;
			ss << health;
			string text;
			ss >> text;
			char s[100] = "Health:";
			for(int i=0;i<text.length();i++)
			{
				s[i+7]=text[i];
			}
			GL3Font.font->Render(s);
			view_mode=0;
			reshapeWindow(window,width,height);
		}
	}

	draw_3d(0,galaxy2,500, -260, 100,M_PI/2.0,glm::vec3(1,0,0));
	draw_3d(0,rectangle,starx,stary, 500,-M_PI/2.0,glm::vec3(0,0,1));
	draw_3d(0,rectangle,starx1,stary1, -330,M_PI/8.0,glm::vec3(1,0,0));
	draw_3d(1,galaxy,-350, 300, 800,M_PI/4.0,glm::vec3(1,0,0));
	draw_3d(1,earth,1400, -100, -225,M_PI/2.0,glm::vec3(0,1,0));
	draw_3d(1,moon,-1500, 100, -325,M_PI/2.0,glm::vec3(0,1,0));

}

/* Initialise glfw window, I/O callbacks and the renderer to use */
/* Nothing to Edit here */
GLFWwindow* initGLFW (int width, int height)
{
	GLFWwindow* window; // window desciptor/handle
	
	glfwSetErrorCallback(error_callback);
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, "Sample OpenGL 3.3 Application", NULL, NULL);

	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	glfwSwapInterval( 1 );

	/* --- register callbacks with GLFW --- */

	/* Register function to handle window resizes */
	/* With Retina display on Mac OS X GLFW's FramebufferSize
	 is different from WindowSize */
	glfwSetFramebufferSizeCallback(window, reshapeWindow);
	glfwSetWindowSizeCallback(window, reshapeWindow);

	/* Register function to handle window close */
	glfwSetWindowCloseCallback(window, quit);

	/* Register function to handle keyboard input */
	glfwSetKeyCallback(window, keyboard);      // general keyboard input
	glfwSetCharCallback(window, keyboardChar);  // simpler specific character handling
	glfwSetScrollCallback(window,cbfun);
	/* Register function to handle mouse click */
	glfwSetMouseButtonCallback(window, mouseButton);  // mouse button clicks

	return window;
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL (GLFWwindow* window, int width, int height)
{
	// Load Textures
	// Enable Texture0 as current texture memory
	glActiveTexture(GL_TEXTURE0);
	// load an image file directly as a new OpenGL texture
	// GLuint texID = SOIL_load_OGL_texture ("beach.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_TEXTURE_REPEATS); // Buggy for OpenGL3
	GLuint textureID = createTexture("star.jpg");
	// check for an error during the load process
	if(textureID == 0 )
		cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

	// Create and compile our GLSL program from the texture shaders
	textureProgramID = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.TexMatrixID = glGetUniformLocation(textureProgramID, "MVP");


	/* Objects should be created before any other gl function and shaders */
	// Create the models
	createTriangle (); // Generate the VAO, VBOs, vertices data & copy into the array buffer
	rectangle = createRectangle (textureID,40,30);
	rectangle1 = createRectangle(textureID,40,30);

	glActiveTexture(GL_TEXTURE0);

	GLuint textureID1 = createTexture("galaxy.jpg");
	// check for an error during the load process
	if(textureID1 == 0 )
		cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

	// Create and compile our GLSL program from the texture shaders
	textureProgramID1 = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.TexMatrixID = glGetUniformLocation(textureProgramID1, "MVP");

	galaxy = createRectangle (textureID1,250,200);

	glActiveTexture(GL_TEXTURE0);

	GLuint textureID2 = createTexture("new.jpg");
	// check for an error during the load process
	if(textureID2 == 0 )
		cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

	// Create and compile our GLSL program from the texture shaders
	textureProgramID2 = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.TexMatrixID = glGetUniformLocation(textureProgramID2, "MVP");

	galaxy2 = createRectangle (textureID2,250,200);

	glActiveTexture(GL_TEXTURE0);

	GLuint textureID3 = createTexture("earth.jpg");
	// check for an error during the load process
	if(textureID3 == 0 )
		cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

	// Create and compile our GLSL program from the texture shaders
	textureProgramID3 = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.TexMatrixID = glGetUniformLocation(textureProgramID3, "MVP");

	earth = createRectangle (textureID3,250,200);

	glActiveTexture(GL_TEXTURE0);

	GLuint textureID4 = createTexture("b.jpg");
	// check for an error during the load process
	if(textureID4 == 0 )
		cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

	// Create and compile our GLSL program from the texture shaders
	textureProgramID4 = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.TexMatrixID = glGetUniformLocation(textureProgramID4, "MVP");

	moon = createRectangle (textureID4,400,200);

	ifstream level;
	char ch;
	int i=0;
	level.open("level0.txt");
	if (level.is_open()) {
		 while (!level.eof()) {
		 	level.get(ch);
		 	if(ch!='\n')
		 		level0[i++]=ch;
    		//level >> output;
		    //cout<<output;


 		}
	}
	level.close();
	i=0;
	level.open("level1.txt");
	if (level.is_open()) {
		 while (!level.eof()) {
		 	level.get(ch);
		 	if(ch!='\n')
		 		level1[i++]=ch;
    		//level >> output;
		    //cout<<output;


 		}
	}
	level.close();
	i=0;
	level.open("level2.txt");
	if (level.is_open()) {
		 while (!level.eof()) {
		 	level.get(ch);
		 	if(ch!='\n')
		 		level2[i++]=ch;
    		//level >> output;
		    //cout<<output;


 		}
	}
	level.close();
	i=0;
	level.open("level3.txt");
	if (level.is_open()) {
		 while (!level.eof()) {
		 	level.get(ch);
		 	if(ch!='\n')
		 		level3[i++]=ch;
    		//level >> output;
		    //cout<<output;


 		}
	}
	level.close();


	for(int i=0;i<100;i++)
	{
		output[i]='0';
		if(output[i]=='c')
		{
			valid[i/10][i%10]=1;
			output[i]=-1+'0';
		}
	}
	//GLfloat basecol [118];
	for(int i=0;i<36;i++)
	{
		if(i>24)
		{
			basecol[3*i]=0.5;
			basecol[3*i+1]=0.7;
			basecol[3*i+2]=0.3;
		}
		else
		{
			basecol[3*i]=132/255.0;
			basecol[3*i+1]=66/255.0;
			basecol[3*i+2]=4/255.0;	
		}
	}
	for(int i=0;i<108;i++)
	{
		white[i]=1;
		grey[i]=0.3;
		black[i]=0;
	}
	for(int i=0;i<36;i++)
	{
		bodyc[3*i]=32/255.0;
		bodyc[3*i+1]=47/255.0;
		bodyc[3*i+2]=56/255.0;
	}

	for(int i=0;i<10;i++)
	{
		for(int j=0;j<10;j++)
		{
				cuboid[i][j]=createCuboid(25,25,25*abs((output[i*10+j])-'0'),basecol);
		}
	}
	rotang=60;
	arrays[0]=glm::vec3(180,200,390);
	arrays[1]=glm::vec3(390,-300,-450);
	arrays[2]=glm::vec3(680,-200,390);
	arrays[3]=glm::vec3(400,200,400);
	arrays[4]=glm::vec3(350,200,-390);
	arrays[5]=glm::vec3(400,-200,390);
	arrays[6]=glm::vec3(-650,200,590);
	arrays[7]=glm::vec3(-180,200,-390);
	arrays[8]=glm::vec3(380,-200,390);
	posx=225,posy=73;posz=-225;
	hair = createCuboid(16,7,16,black);
	eyes = createCuboid(3,3,3,black);
	head = createCuboid(14,14,14,white);
	body = createCuboid(12,12,12,bodyc);
	leg[0]=leg[1]=createCuboid(4,16,4,grey);
	hand[0]=hand[1]=createCuboid(4,16,4,grey);
	shoe[0]= shoe[1] = createCuboid(5,5,5,black);

	centercub=createCuboid(20,20,40,grey);

	orbit1 = createSector(950,90);
	orbit2 = createSector(650,90);
	for(int i=0;i<16000;i+=3)
	{
		red[i]=1;
		red[i+1]=0;
		red[i+2]=0;
		gold[i]=253/255.0;
		gold[i+1]=201/255.0;
		gold[i+2]=0;
		orange[i]=1;
		orange[i+1]=64/255.0;
		orange[i+2]=0;
	}
	rockcub = createCuboid(50,25,25,grey);
	rockhead = create_halfpyramid(25,50,red);
	fire = create_halfpyramid(10,20,orange);
	bigfire = create_halfpyramid(20,45,gold);
	sphere1 = create_sphere(40,red);
	sphere2 = create_sphere(30,gold);
	sun=create_sphere(150,gold);
	star = create_star(10);
	powerup = createPyramid(10,20,gold);
	wincoin = createPyramid(10,20,orange);
	killer = create_halfpyramid(6,30,grey);

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders( "Sample_GL3.vert", "Sample_GL3.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.MatrixID = glGetUniformLocation(programID, "MVP");

	thread(play_audio,"rocket.mp3").detach();
	reshapeWindow (window, width, height);

	// Background color of the scene
	glClearColor (0.0f, 0.0f, 0.0f, 0.0f); // R, G, B, A
	glClearDepth (1.0f);

	glEnable (GL_DEPTH_TEST);
	glDepthFunc (GL_LEQUAL);
//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Initialise FTGL stuff
	//const char* fontfile = "/home/harsha/Classes/Two-2/Graphics/OGL3Sample2D/GLFW/GL3_Fonts_Textures/arial.ttf";
	const char* fontfile = "arial.ttf";
	GL3Font.font = new FTExtrudeFont(fontfile); // 3D extrude style rendering

	if(GL3Font.font->Error())
	{
		cout << "Error: Could not load font `" << fontfile << "'" << endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Create and compile our GLSL program from the font shaders
	fontProgramID = LoadShaders( "fontrender.vert", "fontrender.frag" );
	GLint fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform;
	fontVertexCoordAttrib = glGetAttribLocation(fontProgramID, "vertexPosition");
	fontVertexNormalAttrib = glGetAttribLocation(fontProgramID, "vertexNormal");
	fontVertexOffsetUniform = glGetUniformLocation(fontProgramID, "pen");
	GL3Font.fontMatrixID = glGetUniformLocation(fontProgramID, "MVP");
	GL3Font.fontColorID = glGetUniformLocation(fontProgramID, "fontColor");

	GL3Font.font->ShaderLocations(fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform);
	GL3Font.font->FaceSize(1);
	GL3Font.font->Depth(0);
	GL3Font.font->Outset(0, 0);
	GL3Font.font->CharMap(ft_encoding_unicode);

	cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
	cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
	cout << "VERSION: " << glGetString(GL_VERSION) << endl;
	cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

}

int main (int argc, char** argv)
{
	width = 600;
	height = 600;

	GLFWwindow* window = initGLFW(width, height);

	initGL (window, width, height);

	double last_update_time = glfwGetTime(), current_time;



	/* Draw in loop */
	while (!glfwWindowShouldClose(window)) {

		// OpenGL Draw commands
		draw(window);

		// Swap Frame Buffer in double buffering
		glfwSwapBuffers(window);

		// Poll for Keyboard and mouse events
		glfwPollEvents();


		// Control based on time (Time based transformation like 5 degrees rotation every 0.5s)
		current_time = glfwGetTime(); // Time in seconds
		if ((current_time - last_update_time) >= 0.5) { // atleast 0.5s elapsed since last frame
			// do something every 0.5 seconds ..
			last_update_time = current_time;
		}
	}

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
