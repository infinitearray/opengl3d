all: sample2D

sample2D: a.cpp glad.c
	g++ -w -o sample2D a.cpp glad.c -ldl -lGL -lglfw -lftgl -lSOIL -I/usr/local/include -I/usr/local/include/freetype2 -L/usr/local/lib -lao -lmpg123 -std=gnu++11 -lpthread

clean:
	rm sample2D
